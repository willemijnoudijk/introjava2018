package nl.bioinf.nomi.loose_ends;

import static nl.bioinf.nomi.loose_ends.Outer.StaticInner;

public class InnerClassesDemo {
    public static void main(String[] args) {
        InnerClassesDemo demo = new InnerClassesDemo();
        demo.start();
    }

    private void start() {
        Outer outer = new Outer();
        outer.outerMethod();

        StaticInner staticInner = new StaticInner();
        staticInner.staticInnerMethod();


        outer.useInterfaces();
    }
}
