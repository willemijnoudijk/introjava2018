package nl.bioinf.nomi.loose_ends;

public class Outer {
    private PowerUp powerUpTwo = new PowerUp() {
        @Override
        public void powerUp(int x) {
            System.out.println("" + x * x);
        }
    };
    private PowerUp powerUpThree = new PowerUp() {
        @Override
        public void powerUp(int x) {
            System.out.println("" + x * x * x);
        }
    };

    public void useInterfaces(){
        powerUpTwo.powerUp(2);
        powerUpThree.powerUp(3);
    }

    public void outerMethod() {
        System.out.println("Outer method");

        StaticInner inner = new StaticInner();
        inner.staticInnerMethod();

    }

    public static class StaticInner {
        public void staticInnerMethod() {
            System.out.println("Static Inner Method");
        }
    }

    public class NonStaticInner {
        public void useOuter() {
            Outer.this.useInterfaces();
        }
    }

    private interface PowerUp {
        void powerUp(int x);
    }

}
