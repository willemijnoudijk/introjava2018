package nl.bioinf.nomi.loose_ends;

import javax.swing.text.NumberFormatter;
import java.text.NumberFormat;
import java.util.Locale;

public class FinalDemo {
    public static final int EARTH_RADIUS = 6371;

    private static final int COST_OF_FP = 3;


    public static void main(String[] args) {
        FinalDemo demo = new FinalDemo();
        demo.start();
    }

    private void start() {
        constantDemo();
    }

    private void constantDemo() {
        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
        numberFormat.setMaximumFractionDigits(0);

        String myData = "";
        final int costOfFn = 4;

        doTraining(myData, COST_OF_FP, costOfFn);

        System.out.println("the earth radius is " + EARTH_RADIUS + " km");

        //calculate the earth circumference
        //2 pi r

        double circumference = 2 * Math.PI * EARTH_RADIUS;

        System.out.println("The circumference = " + circumference + " km");
        System.out.println("The circumference = " + numberFormat.format(circumference) + " km");
    }

    public final void doTraining( String myData, final int costOfFp, final int costOfFn) {
    }

}
