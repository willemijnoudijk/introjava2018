package nl.bioinf.nomi.exceptions;

import java.io.IOException;

public class ExceptionsDemo {
    public static void main(String[] args) {
        foo();
        System.out.println("doei");
        try {

        } catch (OutOfMemoryError err) {
            //trying to solve it
        }
    }

    private static void foo() {
        try {
            bar();
        } catch (ArithmeticException | IOException ex) { //this is called a Multi-catch
            System.out.println("caught an exception");
        }
        System.out.println("hallo");
    }

    private static int bar() throws IOException{
        System.out.println("moi");
        boolean foo = true;
        if(foo) throw new IOException("gotcha!");
        return 42/0;
    }

}
