package nl.bioinf.nomi.obj_construction;

import java.time.LocalDate;
import java.util.Date;

public class Student extends Object{
    private String name;
    private int studentId;
    private Major major;
    private LocalDate birthDate;

    public Student(String name, int studentId, Major major, LocalDate birthDate) {
        super();
        this.name = name;
        this.studentId = studentId;
        this.major = major;
        this.birthDate = birthDate;
    }

    public Student(String name, int studentId, LocalDate birthDate) {
        super(); //calls constructor of super class
        this.name = name;
        this.studentId = studentId;
        this.birthDate = birthDate;
        this.major = new Major("UNKNOWN");
    }

    public int getStudentId() {
        return studentId;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Major getMajor() {
        return major;
    }

    public void setMajor(Major major) {
        this.major = major;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", studentId=" + studentId +
                ", major=" + major +
                ", birthDate=" + birthDate +
                '}';
    }
}
