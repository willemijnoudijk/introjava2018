package nl.bioinf.nomi.obj_construction;

import nl.bioinf.nomi.hello.User;
import java.time.LocalDate;

public class StudentOffice {
    public static void main(String[] args) {
        StudentOffice office = new StudentOffice();
        office.start();
    }

    private void start() {

        Student student1 = new Student(
                "Henk",
                12345,
                new Major("BFV"),
                LocalDate.parse("2001-05-23")); //Locale dependent??

        System.out.println("student1 = " + student1);

        Student student2 = new Student(
                "Roos",
                23456,
                LocalDate.parse("2001-05-23")); //Locale dependent??

        System.out.println("student2 = " + student2);

        System.out.println("student2.getMajor().getName() = " + student2.getMajor().getName());


        for (School school: School.values()) {
            System.out.println("school = " + school);
        }

        User u = null;

    }

}
