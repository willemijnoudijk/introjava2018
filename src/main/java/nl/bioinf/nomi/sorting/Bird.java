package nl.bioinf.nomi.sorting;

public class Bird implements Flyable {
    @Override
    public void fly() {
        System.out.println("Bird, flying");
    }
}
