package nl.bioinf.nomi.sorting;

import nl.bioinf.nomi.hello.Horse;

import java.util.Comparator;

public class FurComparator implements Comparator<Horse> {
    @Override
    public int compare(Horse o1, Horse o2) {
        //delegate to class String
        int comp = o1.furColor.compareTo(o2.furColor);
        if (comp == 0) {
            return Integer.compare(o1.weightInKilograms, o2.weightInKilograms);
//            return o1.compareTo(o2);
        } else {
            return comp;
        }
    }
}
