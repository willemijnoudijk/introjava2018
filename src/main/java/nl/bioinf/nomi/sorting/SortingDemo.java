package nl.bioinf.nomi.sorting;

import nl.bioinf.nomi.collections_demo.CollectionsDemo;
import nl.bioinf.nomi.hello.Horse;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SortingDemo {
    public static void main(String[] args) {
        List<Horse> horses = CollectionsDemo.createHorseList();

        System.out.println("horses = " + horses);
        //sorts on weight
        Collections.sort(horses);

        System.out.println("horses = " + horses);

        //sorts on color and weight
        Collections.sort(horses, new FurComparator());

        System.out.println("horses = " + horses);

        //sorts on name with anonymous inner class
        Collections.sort(horses, new Comparator<Horse>() {
            @Override
            public int compare(Horse o1, Horse o2) {
                return o1.name.compareTo(o2.name);
            }
        });

        System.out.println("horses = " + horses);
    }

}
