package nl.bioinf.nomi.testtube;

import java.util.Properties;

public class TestTube {
    public static void main(String[] args) {
        System.out.println("Starting simulation...");

        if (args.length != 1) {
            System.err.println("nu type provided!");
            return;
        }
        if (isCorrectType(args[0])) {
            /*Get out of static ASAP!*/
            TestTube tube = new TestTube();
            tube.startSimulation(args[0]);
        } else {
            System.err.println("No valid type to grow: " + args[0]);
        }
    }

    private static boolean isCorrectType(String arg) {
        if (arg.equals("bacterium") || arg.equals("eukaryote")) {
            return true;
        }
        return false;
    }

    private void startSimulation(String cellType) {
        Cell cell = getCell(cellType);
        for (int generation = 0; generation < 10; generation++) {
            //How many objects have been created in this app at this point?
            Cell newCell = cell.grow();
            if (newCell != null) {
                System.out.println("new Cell formed= " + newCell);
            }
            if (generation % 3 == 0 && cell instanceof Bacterium) {
                Bacterium bact = (Bacterium)cell;
                bact.causePandemic();
            }
        }
    }

    private Cell getCell(String cellType) {
        Cell cell = null;
        if (cellType.equals("bacterium")) {
            cell = new Bacterium();
        } else if (cellType.equals("eukaryote")) {
            cell = new Eukaryote();
        }
        return cell;
    }
}
