package nl.bioinf.nomi.testtube;

public abstract class Cell {
    //inner state; do not touch this (Python `_size`)
    protected String name = "Generic Cell";
    private int size;
    private int maxSize;

    public Cell() {}


    public int getMaxSize() {
        return maxSize;
    }

    protected void setMaxSize(int newMaxSize) {
        this.maxSize = newMaxSize;
    }

    public int getSize() {
        return this.size;
    }

    protected void setSize(int newsize) { //protected: package & subclasses visible
        if (newsize <= 0 || newsize > getMaxSize()) {
            throw new IllegalArgumentException("Illegal cell size attempted: " + newsize
                    + "; maxSize = " + this.getMaxSize());
        }
        this.size = newsize;
    }

    //should be implemented by subclasses
    public abstract Cell grow();

    @Override
    public String toString() {
        return "Cell{" +
                "size=" + size +
                ", maxSize=" + maxSize +
                '}';
    }
}
