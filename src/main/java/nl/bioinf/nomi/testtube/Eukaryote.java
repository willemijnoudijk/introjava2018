package nl.bioinf.nomi.testtube;

public class Eukaryote extends Cell {
    public Eukaryote() {
        this.setMaxSize(50);
        this.setSize(20);
    }

    @Override
    public Cell grow() {
        System.out.println("grow like a eukaryote" );
        this.setSize(this.getSize() + 8);
        if (this.getSize() >= this.getMaxSize()) {
            this.setSize(20);
            return new Eukaryote();
        }
        return null;    }
}
