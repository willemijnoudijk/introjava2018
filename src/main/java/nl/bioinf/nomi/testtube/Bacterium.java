package nl.bioinf.nomi.testtube;

public class Bacterium extends Cell {
    public Bacterium() { //same as __init__(self)
        this.setMaxSize(30);
        this.setSize(10);
    }

    @Override
    public Cell grow() {
        System.out.println("bacteroium growing..size = " + this.getSize());
        this.setSize(this.getSize() + 5);
//        size += 5;
        if (this.getSize() >= this.getMaxSize()) {
            this.setSize(10);
            return new Bacterium();
        }
        return null;
    }

    public void causePandemic() {
        System.out.println("Oh my God we are all going to die!");
    }
}
