package nl.bioinf.nomi.collections_demo;

import nl.bioinf.nomi.hello.Horse;

import java.util.*;

public class CollectionsDemo {
    public static void main(String[] args) {
        List<Horse> horses = createHorseList();


        System.out.println("horse 2 = " + horses.get(1));
        horses.remove(1);

        HashSet<Horse> horseSet = new HashSet<>();
        horseSet.addAll(horses);

        Horse horse3 = new Horse();
        horse3.furColor = "brown";
        horse3.weightInKilograms = 644;

        Horse horse4 = new Horse();
        horse4.furColor = "brown";
        horse4.weightInKilograms = 644;
        horseSet.add(horse4);

        System.out.println("horseSet.contains(horse3) = " + horseSet.contains(horse3));

        horse4.furColor = "red";

        System.out.println("horseSet.contains(horse4) = " + horseSet.contains(horse4));

        System.out.println("horseSet = " + horseSet);


        System.out.println("horseSet = " + horseSet);

        HashMap<Integer, String> collection = new HashMap<>();
        collection.put(42, "The answer to everything"); //autoboxing
        collection.put(Integer.valueOf(666), "The number of the beast");
        collection.put(new Integer(0), "Nothing");

        boolean b = collection.containsKey(44);
        System.out.println("b = " + b);
        System.out.println("collection = " + collection.get(666)); //autoboxing

        for(Map.Entry entry : collection.entrySet()) {
            System.out.println("entry = " + entry);

        }
    }

    public static List<Horse> createHorseList() {
        Horse horse1 = new Horse();
        horse1.furColor = "white";
        horse1.weightInKilograms = 555;
        horse1.name = "FastMF";

        Horse horse2 = new Horse();
        horse2.furColor = "black";
        horse2.weightInKilograms = 732;
        horse2.name = "Tinkerbell";

        Horse horse3 = new Horse();
        horse3.furColor = "brown";
        horse3.weightInKilograms = 644;
        horse3.name = "FasterMF";

        Horse horse4 = new Horse();
        horse4.furColor = "brown";
        horse4.weightInKilograms = 731;
        horse4.name = "DavidBowie";

        Horse horse5 = new Horse();
        horse5.furColor = "brown";
        horse5.weightInKilograms = 801;
        horse5.name = "FactOfLife";

        ArrayList<Horse> horses = new ArrayList<>();
        horses.add(horse1);
        horses.add(horse2);
        horses.add(horse3);
        horses.add(horse4);
        horses.add(horse5);

//        List<Horse> horses = List.of(horse1, horse2, horse3, horse4);
        return horses;
    }
}
