package nl.bioinf.nomi.methods_demo;

public class ShapeAnalysis {

    public static double getSurfaceAreaOfCircle(double radius) {
        double area = Math.PI * Math.pow(radius, 2);
//        double area = Math.PI * radius * radius; //same
        return area;
    }


}
