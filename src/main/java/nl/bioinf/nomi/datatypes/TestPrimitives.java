package nl.bioinf.nomi.datatypes;

public class TestPrimitives {
    static int counter;
    //same as
    //static int counter = 0;

    public static void main(String[] args) {

        byte b = 0;
//        for (int x = 0; x <= 300; x++) {
//            System.out.println("b = " + b);
//            b++;
//        }

        int counter2;
        System.out.println("counter = " + counter);
        counter2 = 9;
        System.out.println("counter2 = " + counter2);

        char n = 'n';
        long l = n;
        System.out.println("l = " + l);

        n++;
        System.out.println("n = " + n);

//        String s = "foo";
//        System.out.println(s.codePointAt(0));

        String name = "Henk";
        name = name.toUpperCase();
        System.out.println("name = " + name);

        if (name == "HENK") { //should use name.equals("HENK")
            System.out.println("same?");
        }

        String foo = null;
//        foo.toUpperCase(); //commented out because of NullPointerException

        String[] words = new String[4];
//        words[0].toUpperCase(); //commented out because of NullPointerException

        //demo ternary operator
        int z = 20;
        String label = (z < 30) ? "young" : "old";

        //auto increment/decrement operators

        System.out.println("z = " + z++);
        System.out.println("z = " + ++z);

        int d = --z + z++ + ++z;
        System.out.println("d = " + d);
    }
}
