package nl.bioinf.nomi.datatypes;

import java.util.Arrays;

public class CommandLineArgs {
    public static void main(String[] args) {
        System.out.println("args = " + args);
        System.out.println("args.length = " + args.length);
        System.out.println(Arrays.toString(args));
        for (String arg : args) {
            System.out.println("arg = " + arg);
        }
    }
}
