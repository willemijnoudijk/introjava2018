package nl.bioinf.nomi.hello;


import java.util.Objects;

/**
 * Models a Horse. you know, found in sausages.
 * @author Michiel Noback
 */
public class Horse implements Comparable<Horse> {
    public String name;
    public int id;
    public int weightInKilograms;
    public String furColor = "brown";

    /**
     * Will make the horse gallop at the given speed.
     * @param speedInKmPerHour the speed
     */
    public void gallop(double speedInKmPerHour) {
        //simulate galloping
        System.out.println("Horse galloping at " + speedInKmPerHour + " km per hour");
    }

    @Override
    public String toString() {
        return "\n\tHorse: {color = " + furColor + ", weight = " + weightInKilograms + ", name=" + name + "}";
    }

    //return:
    //0 if equal
    //<0 if this < other
    //>0 if this > other
    @Override
    public int compareTo(Horse o) {
        //OK but verbose
//        if (this.weightInKilograms == o.weightInKilograms) {
//            return 0;
//        } else if (this.weightInKilograms < o.weightInKilograms) {
//            return -1;
//        } else {
//            return 1;
//        }

        //unsafe with very big positive / negative numbers
//        return o.weightInKilograms - this.weightInKilograms;

        //Best: use delegation to the type that already knows best
        return Integer.compare(this.weightInKilograms, o.weightInKilograms);
    }

    //    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        Horse horse = (Horse) o;
//        return weightInKilograms == horse.weightInKilograms &&
//                Objects.equals(furColor, horse.furColor);
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hash(weightInKilograms, furColor);
//    }
}
